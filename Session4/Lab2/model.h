#define SINGLE_FILE
/**
  ******************************************************************************
  * @file    defines.hh
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, Université Côte d'Azur, LEAT, France
  * @version 2.1.0
  * @date    10 january 2024
  * @brief   Global C pre-processor definitions to use to build all source files (incl. CMSIS-NN)
  */

/* CMSIS-NN round mode definition */
#if defined(WITH_CMSIS_NN) || defined(WITH_NMSIS_NN)


#define ARM_NN_TRUNCATE 1
#define RISCV_NN_TRUNCATE 1

#endif // defined(WITH_CMSIS_NN) || defined(WITH_NMSIS_NN)
/**
  ******************************************************************************
  * @file    number.hh
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version 1.0.0
  * @date    2 february 2021
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */

#ifdef __cplusplus
extern "C" {
#endif

#ifndef __NUMBER_H__
#define __NUMBER_H__

#include <stdint.h>
#include <stddef.h>
#include <math.h>

#ifdef WITH_CMSIS_NN
#include "arm_nnfunctions.h"
#endif

#define _clamp_to(type, number) clamp_to_number_t_ ## type (number)
#define clamp_to(type, number) _clamp_to(type, number)
#define _scale(type, number, scale_factor, round_mode) scale_number_t_ ## type (number, scale_factor, round_mode)
#define scale(type, number, scale_factor, round_mode) _scale(type, number, scale_factor, round_mode)
#define _scale_and_clamp_to(type, number, scale_factor, round_mode) scale_and_clamp_to_number_t_ ## type (number, scale_factor, round_mode)
#define scale_and_clamp_to(type, number, scale_factor, round_mode) _scale_and_clamp_to(type, number, scale_factor, round_mode)

typedef enum {
  ROUND_MODE_NONE,
  ROUND_MODE_FLOOR,
  ROUND_MODE_NEAREST,
} round_mode_t;

// Idea 1: Write the smallest min max interval of the net, could be an issue for hybrid int type network
// Idea 2: listing any interval and add type in name in a switch case like <- better but painfull
// #define NUMBER_MIN		// Max value for this numeric type
// #define NUMBER_MAX		// Min value for this numeric type

// // Idea 1: List of all types and write any corresponding function 
// typedef  number_t;		// Standard size numeric type used for weights and activations
// typedef  long_number_t;	// Long numeric type used for intermediate results

#define NUMBER_MIN_INT16_T -32768
#define NUMBER_MAX_INT16_T 32767

static inline int32_t min_int16_t(
    int32_t a,
    int32_t b) {
	if (a <= b)
		return a;
	return b;
}

static inline int32_t max_int16_t(
    int32_t a,
    int32_t b) {
	if (a >= b)
		return a;
	return b;
}

static inline int32_t scale_number_t_int16_t(
  int32_t number, int scale_factor, round_mode_t round_mode) {
  if (scale_factor <= 0) {
    // No rounding to apply when shifting left
    return number << - scale_factor;
  } else {
    if (round_mode == ROUND_MODE_NEAREST) {
      number += (1 << (scale_factor - 1)); // +0.5 in fixed-point
    }
    return number >> scale_factor;
  }
}
static inline int16_t clamp_to_number_t_int16_t(
  int32_t number) {
	return (int16_t) max_int16_t(
      NUMBER_MIN_INT16_T,
      min_int16_t(
        NUMBER_MAX_INT16_T, number));
}
static inline int16_t scale_and_clamp_to_number_t_int16_t(
  int32_t number, int scale_factor, round_mode_t round_mode) {
#ifdef WITH_CMSIS_NN
  // Not really CMSIS-NN but use SSAT anyway
  if (scale_factor <= 0) {
    // No rounding to apply when shifting left
    return __SSAT(number << - scale_factor, sizeof(int16_t) * 8);
  } else {
    if (round_mode == ROUND_MODE_NEAREST) {
      number += (1 << (scale_factor - 1)); // +0.5 in fixed-point
    }
    return __SSAT(number >> scale_factor, sizeof(int16_t) * 8);
  }
#else
  number = scale_number_t_int16_t(number, scale_factor, round_mode);
  return clamp_to_number_t_int16_t(number);
#endif
}

#define NUMBER_MIN_INT32_T -2147483648
#define NUMBER_MAX_INT32_T 2147483647

static inline int64_t min_int32_t(
    int64_t a,
    int64_t b) {
	if (a <= b)
		return a;
	return b;
}

static inline int64_t max_int32_t(
    int64_t a,
    int64_t b) {
	if (a >= b)
		return a;
	return b;
}

static inline int64_t scale_number_t_int32_t(
  int64_t number, int scale_factor, round_mode_t round_mode) {
  if (scale_factor <= 0) {
    // No rounding to apply when shifting left
    return number << - scale_factor;
  } else {
    if (round_mode == ROUND_MODE_NEAREST) {
      number += (1 << (scale_factor - 1)); // +0.5 in fixed-point
    }
    return number >> scale_factor;
  }
}
static inline int32_t clamp_to_number_t_int32_t(
  int64_t number) {
	return (int32_t) max_int32_t(
      NUMBER_MIN_INT32_T,
      min_int32_t(
        NUMBER_MAX_INT32_T, number));
}
static inline int32_t scale_and_clamp_to_number_t_int32_t(
  int64_t number, int scale_factor, round_mode_t round_mode) {
#ifdef WITH_CMSIS_NN
  // Not really CMSIS-NN but use SSAT anyway
  if (scale_factor <= 0) {
    // No rounding to apply when shifting left
    return __SSAT(number << - scale_factor, sizeof(int32_t) * 8);
  } else {
    if (round_mode == ROUND_MODE_NEAREST) {
      number += (1 << (scale_factor - 1)); // +0.5 in fixed-point
    }
    return __SSAT(number >> scale_factor, sizeof(int32_t) * 8);
  }
#else
  number = scale_number_t_int32_t(number, scale_factor, round_mode);
  return clamp_to_number_t_int32_t(number);
#endif
}




static inline void int64_t_to_float(int64_t * tabint, float * tabfloat, long tabdim, int scale_factor){
  for (int i=0; i<tabdim; i++){
    tabfloat[i] = (float)tabint[i] / (1<<scale_factor);
  }
}

static inline void int32_t_to_float(int32_t * tabint, float * tabfloat, long tabdim, int scale_factor){
  for (int i=0; i<tabdim; i++){
    tabfloat[i] = (float)tabint[i] / (1<<scale_factor);
  }
}

static inline void int16_t_to_float(int16_t * tabint, float * tabfloat, long tabdim, int scale_factor){
  for (int i=0; i<tabdim; i++){
    tabfloat[i] = ((float)tabint[i]) / (1<<scale_factor);
  }
}

static inline void int8_t_to_float(int8_t * tabint, float * tabfloat, long tabdim, int scale_factor){
  for (int i=0; i<tabdim; i++){
    tabfloat[i] = ((float)tabint[i]) / (1<<scale_factor);
  }
}
#endif //__NUMBER_H__

#ifdef __cplusplus
} // extern "C"
#endif
/**
  ******************************************************************************
  * @file    conv1d.hh
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version V2.0
  * @date    24 january 2023
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */

#ifndef _CONV1D_H_
#define _CONV1D_H_

#ifndef SINGLE_FILE
#include "number.h"
#endif

#define INPUT_CHANNELS      9
#define INPUT_SAMPLES       128
#define CONV_FILTERS        2
#define CONV_KERNEL_SIZE    3
#define CONV_STRIDE         1

#define ZEROPADDING_LEFT    0
#define ZEROPADDING_RIGHT   0

#define CONV_OUTSAMPLES     ( ( (INPUT_SAMPLES - CONV_KERNEL_SIZE + ZEROPADDING_LEFT + ZEROPADDING_RIGHT) / CONV_STRIDE ) + 1 )

typedef int16_t conv1d_output_type[CONV_OUTSAMPLES][CONV_FILTERS];

#if 0
void conv1d(
  const number_t input[INPUT_SAMPLES][INPUT_CHANNELS],                    // IN
  const number_t kernel[CONV_FILTERS][CONV_KERNEL_SIZE][INPUT_CHANNELS],  // IN

  const number_t bias[CONV_FILTERS],						                          // IN

  number_t output[CONV_OUTSAMPLES][CONV_FILTERS]);                       // OUT
#endif

#undef INPUT_CHANNELS
#undef INPUT_SAMPLES
#undef CONV_FILTERS
#undef CONV_KERNEL_SIZE
#undef CONV_STRIDE
#undef ZEROPADDING_LEFT
#undef ZEROPADDING_RIGHT
#undef CONV_OUTSAMPLES

#endif//_CONV1D_H_
/**
  ******************************************************************************
  * @file    conv.cc
  * @author  Sébastien Bilavarn, LEAT, CNRS, Université Côte d'Azur, France
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version V2.0
  * @date    24 march 2020
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */

#ifndef SINGLE_FILE
#include "conv1d.h"
#include "number.h"
#endif

#ifdef WITH_CMSIS_NN
#include "arm_nnfunctions.h"
#elif defined(WITH_NMSIS_NN)
#include "riscv_nnfunctions.h"
#endif

#define INPUT_CHANNELS      9
#define INPUT_SAMPLES       128
#define CONV_FILTERS        2
#define CONV_KERNEL_SIZE    3
#define CONV_STRIDE         1

#define ZEROPADDING_LEFT    0
#define ZEROPADDING_RIGHT   0

#define CONV_OUTSAMPLES     ( ( (INPUT_SAMPLES - CONV_KERNEL_SIZE + ZEROPADDING_LEFT + ZEROPADDING_RIGHT) / CONV_STRIDE ) + 1 )

#define ACTIVATION_RELU

// For fixed point quantization
#define WEIGHTS_SCALE_FACTOR 9
#define INPUT_SCALE_FACTOR 9
#define OUTPUT_SCALE_FACTOR 9
#define OUTPUT_ROUND_MODE ROUND_MODE_FLOOR
#define NUMBER_T int16_t
#define LONG_NUMBER_T int32_t


static inline void conv1d(
  const NUMBER_T input[INPUT_SAMPLES][INPUT_CHANNELS],                    // IN
  const NUMBER_T kernel[CONV_FILTERS][CONV_KERNEL_SIZE][INPUT_CHANNELS],  // IN

  const NUMBER_T bias[CONV_FILTERS],						                          // IN

  NUMBER_T output[CONV_OUTSAMPLES][CONV_FILTERS]) {                       // OUT

#if !defined(WITH_CMSIS_NN) && !defined(WITH_NMSIS_NN)
  unsigned short pos_x, z, k; 	// loop indexes for output volume
  unsigned short x;
  int input_x;
  LONG_NUMBER_T output_acc;

  for (pos_x = 0; pos_x < CONV_OUTSAMPLES; pos_x++) { 
    for (k = 0; k < CONV_FILTERS; k++) { 

      output_acc = scale(NUMBER_T, (LONG_NUMBER_T)bias[k], -INPUT_SCALE_FACTOR, OUTPUT_ROUND_MODE);


      for (x = 0; x < CONV_KERNEL_SIZE; x++) {
        input_x = pos_x * CONV_STRIDE - ZEROPADDING_LEFT + x;

        if (input_x >= 0 && input_x < INPUT_SAMPLES) { // ZeroPadding1D
          for (z = 0; z < INPUT_CHANNELS; z++) {
            output_acc += (LONG_NUMBER_T)input[input_x][z] * (LONG_NUMBER_T)kernel[k][x][z];
          }
        }
      }
      
#ifdef ACTIVATION_LINEAR
      output[pos_x][k] = scale_and_clamp_to(NUMBER_T, output_acc, INPUT_SCALE_FACTOR + WEIGHTS_SCALE_FACTOR - OUTPUT_SCALE_FACTOR, OUTPUT_ROUND_MODE);
#elif defined(ACTIVATION_RELU)
      // Activation function: ReLU
      if (output_acc < 0) {
        output[pos_x][k] = 0;
      } else {
        output[pos_x][k] = scale_and_clamp_to(NUMBER_T, output_acc, INPUT_SCALE_FACTOR + WEIGHTS_SCALE_FACTOR - OUTPUT_SCALE_FACTOR, OUTPUT_ROUND_MODE);
      }
#endif
    }
  }

#else


  static q15_t bufferA[2*INPUT_CHANNELS*CONV_KERNEL_SIZE];
#if INPUT_CHANNELS % 2 == 0 && CONV_FILTERS % 2 == 0
#ifdef WITH_CMSIS_NN
  arm_convolve_HWC_q15_fast_nonsquare(
#elif defined(WITH_NMSIS_NN)
  riscv_convolve_HWC_q15_fast_nonsquare(
#endif
#else
#ifdef WITH_CMSIS_NN
  arm_convolve_HWC_q15_basic_nonsquare(
#elif defined(WITH_NMSIS_NN)
  riscv_convolve_HWC_q15_basic_nonsquare(
#endif
#endif
                                      (q15_t*)input, //Im_in
                                      INPUT_SAMPLES, //dim_im_in_x
                                      1, //dim_im_in_y
                                      INPUT_CHANNELS, //ch_im_in
                                      (q15_t*)kernel, //wt
                                      CONV_FILTERS, //ch_im_out
                                      CONV_KERNEL_SIZE, //dim_kernel_x
                                      1, //dim_kernel_y
                                      ZEROPADDING_LEFT, //padding_x, left and right must be equal
                                      0, //padding_y
                                      CONV_STRIDE, //stride_x
                                      1, //stride_y
                                      (q15_t*)bias, //bias
                                      INPUT_SCALE_FACTOR, //bias_shift
                                      INPUT_SCALE_FACTOR + WEIGHTS_SCALE_FACTOR - OUTPUT_SCALE_FACTOR, //out_shift
                                      (q15_t*)output, //Im_out
                                      CONV_OUTSAMPLES, //dim_im_out_x
                                      1, //dim_im_out_y
                                      bufferA, //bufferA
                                      NULL //bufferB, unused
                                      );
#ifdef ACTIVATION_RELU
#ifdef WITH_CMSIS_NN
  arm_relu_q15((q15_t*)output, CONV_FILTERS * CONV_OUTSAMPLES);
#elif defined(WITH_NMSIS_NN)
  riscv_relu_q15((q15_t*)output, CONV_FILTERS * CONV_OUTSAMPLES);
#endif
#endif


#endif
}

#undef INPUT_CHANNELS
#undef INPUT_SAMPLES
#undef CONV_FILTERS
#undef CONV_KERNEL_SIZE
#undef CONV_STRIDE
#undef ZEROPADDING_LEFT
#undef ZEROPADDING_RIGHT
#undef CONV_OUTSAMPLES
#undef ACTIVATION_RELU
#undef WEIGHTS_SCALE_FACTOR
#undef INPUT_SCALE_FACTOR
#undef OUTPUT_SCALE_FACTOR
#undef NUMBER_T
#undef LONG_NUMBER_T
/**
  ******************************************************************************
  * @file    weights/conv1d.cc
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version 1.0.0
  * @date    24 march 2020
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */

#include <stdint.h>

#define INPUT_CHANNELS    9
#define CONV_FILTERS      2
#define CONV_KERNEL_SIZE  3


const int16_t  conv1d_bias[CONV_FILTERS] = {-70, -125}
;

const int16_t  conv1d_kernel[CONV_FILTERS][CONV_KERNEL_SIZE][INPUT_CHANNELS] = {{{278, -21, 195, 188, 247, -76, 264, 248, 207}
, {18, 87, -51, -64, -311, -138, -127, -365, 87}
, {-162, 99, -117, 257, 313, 120, 256, 226, 57}
}
, {{-211, -309, -26, -246, -181, -299, -118, -360, 68}
, {-127, -168, -158, -18, -273, -275, -240, 79, -285}
, {105, -59, -40, 165, -146, 176, -204, 178, -55}
}
}
;

#undef INPUT_CHANNELS
#undef CONV_FILTERS
#undef CONV_KERNEL_SIZE
/**
  ******************************************************************************
  * @file    flatten.hh
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version V2.0
  * @date    24 january 2023
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */

#ifndef _FLATTEN_H_
#define _FLATTEN_H_

#ifndef SINGLE_FILE
#include "number.h"
#endif

#define OUTPUT_DIM 252

typedef int16_t flatten_output_type[OUTPUT_DIM];

#if 0
void flatten(
  const number_t input[126][2], 			      // IN
	number_t output[OUTPUT_DIM]); 			                // OUT
#endif

#undef OUTPUT_DIM

#endif//_FLATTEN_H_
/**
  ******************************************************************************
  * @file    flatten.cc
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version 2.0.0
  * @date    26 november 2021
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */

#ifndef SINGLE_FILE
#include "flatten.h"
#include "number.h"
#endif

#define OUTPUT_DIM 252

#define NUMBER_T int16_t
#define LONG_NUMBER_T int32_t

static inline void flatten(
  const NUMBER_T input[126][2], 			      // IN
	NUMBER_T output[OUTPUT_DIM]) {			                // OUT

  NUMBER_T *input_flat = (NUMBER_T *)input;

  // Copy data from input to output only if input and output don't point to the same memory address already
  if (input_flat != output) {
    for (size_t i = 0; i < OUTPUT_DIM; i++) {
      output[i] = input_flat[i];
    }
  }
}

#undef OUTPUT_DIM
#undef NUMBER_T
#undef LONG_NUMBER_T
/**
  ******************************************************************************
  * @file    fc.hh
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version V2.0
  * @date    24 january 2023
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */

#ifndef _DENSE_H_
#define _DENSE_H_

#ifndef SINGLE_FILE
#include "number.h"
#include <stdint.h>
#endif

#define INPUT_SAMPLES 252
#define FC_UNITS 6

typedef int16_t dense_output_type[FC_UNITS];

#if 0
void dense(
  const number_t input[INPUT_SAMPLES], 			      // IN
	const number_t kernel[FC_UNITS][INPUT_SAMPLES],  // IN

	const number_t bias[FC_UNITS],			              // IN

	number_t output[FC_UNITS]); 			                // OUT
#endif

#undef INPUT_SAMPLES
#undef FC_UNITS

#endif//_DENSE_H_
/**
  ******************************************************************************
  * @file    fc.cc
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version 1.0.0
  * @date    24 march 2020
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */

#ifndef SINGLE_FILE
#include "dense.h"
#include "number.h"
#endif

#ifdef WITH_CMSIS_NN
#include "arm_nnfunctions.h"
#elif defined(WITH_NMSIS_NN)
#include "riscv_nnfunctions.h"
#endif

#define INPUT_SAMPLES 252
#define FC_UNITS 6
#define ACTIVATION_LINEAR

// For fixed point quantization
#define WEIGHTS_SCALE_FACTOR 9
#define INPUT_SCALE_FACTOR 9
#define OUTPUT_SCALE_FACTOR 9
#define OUTPUT_ROUND_MODE ROUND_MODE_FLOOR
#define NUMBER_T int16_t
#define LONG_NUMBER_T int32_t


static inline void dense(
  const NUMBER_T input[INPUT_SAMPLES], 			      // IN
	const NUMBER_T kernel[FC_UNITS][INPUT_SAMPLES],  // IN

	const NUMBER_T bias[FC_UNITS],			              // IN

	NUMBER_T output[FC_UNITS]) {			                // OUT

#if !defined(WITH_CMSIS_NN) && !defined(WITH_NMSIS_NN)
  unsigned short k, z; 
  LONG_NUMBER_T output_acc;

  //TODO: Fill algorithm for Fully Connected layer computation

#else


  static q15_t bufferA[INPUT_SAMPLES];
#ifdef WITH_CMSIS_NN
  arm_fully_connected_q15(
#elif defined(WITH_NMSIS_NN)
  riscv_fully_connected_q15(
#endif
                             (q15_t*)input,
                             (q15_t*)kernel,
                             INPUT_SAMPLES,
                             FC_UNITS,
                             INPUT_SCALE_FACTOR,
                             INPUT_SCALE_FACTOR + WEIGHTS_SCALE_FACTOR - OUTPUT_SCALE_FACTOR,
                             (q15_t*)bias,
                             (q15_t*)output,
                             (q15_t*)bufferA);
#ifdef ACTIVATION_RELU
#ifdef WITH_CMSIS_NN
  arm_relu_q15((q15_t*)output, FC_UNITS);
#elif defined(WITH_NMSIS_NN)
  riscv_relu_q15((q15_t*)output, FC_UNITS);
#endif
#endif


#endif
}

#undef INPUT_SAMPLES
#undef FC_UNITS
#undef ACTIVATION_LINEAR
#undef WEIGHTS_SCALE_FACTOR
#undef INPUT_SCALE_FACTOR
#undef OUTPUT_SCALE_FACTOR
#undef NUMBER_T
#undef LONG_NUMBER_T
/**
  ******************************************************************************
  * @file    weights/fc.cc
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version 1.0.0
  * @date    24 march 2020
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */

#include <stdint.h>

#define INPUT_SAMPLES 252
#define FC_UNITS 6


const int16_t dense_bias[FC_UNITS] = {27, -76, -86, 65, 64, 139}
;

const int16_t dense_kernel[FC_UNITS][INPUT_SAMPLES] = {{-130, -558, -89, -398, -72, -479, 21, -430, -40, -412, -141, -501, -186, -468, -126, -385, 0, -419, -90, -616, 64, -548, -125, -430, -308, -509, -300, -17, -74, 49, 15, 66, 108, 262, 61, 35, -46, 89, 112, 74, 227, 33, 227, 151, 173, 114, 137, 204, 139, 47, 221, 35, 195, -62, -13, -112, 104, -196, -37, -323, -76, -165, -218, -260, -70, -192, 28, -189, 82, -289, 17, -336, 76, -397, 41, -264, -58, -337, 5, -119, 156, -122, 168, -189, 37, 53, -48, 4, -37, -9, -54, -126, -32, -94, -43, -85, -29, -36, -38, -72, 9, -86, -27, -66, -84, -9, -64, -53, -55, 44, 113, -94, 61, 44, 146, -19, 143, -43, 48, 50, 80, -22, 118, 50, 80, -44, 212, -2, 144, 5, 196, 18, 58, 101, 96, -46, 139, -2, 79, 29, 93, 150, 159, -177, 139, -55, 135, -64, 158, -30, 135, 2, 179, 58, 110, 91, 159, -135, 138, -165, 168, -41, 263, -166, 282, -70, 27, 15, -138, -222, -35, -50, 23, -90, 27, -36, 31, -79, -37, -74, 68, 15, 51, -133, 41, -131, 80, 4, 63, -47, 21, 12, -7, -70, 68, -72, 7, -33, 16, -216, -81, -79, -261, -14, -144, 82, -307, 45, -324, 21, -208, 34, -206, 83, -42, -17, -231, -28, -148, -17, -241, -48, -300, -28, -319, -20, -139, -116, -58, -5, -51, -58, -190, -68, 2, 76, -219, -58, -119, -19, -84, -12, 8, -58, -124, -83, -120, 13, -83, -42, -80, -2}
, {-128, 119, -208, 119, -220, 225, -145, 225, -70, 231, -48, 249, -123, 115, -135, 126, -180, 98, -212, 94, -175, 205, -125, 139, 93, 222, -10, -66, -153, -25, -126, 74, -56, 237, -63, 232, -111, 213, -81, 89, -148, 40, -86, 3, -93, 45, -69, 77, -6, 111, -189, 65, -12, 144, 35, 80, -156, 135, -15, 210, -56, 247, -19, 255, -70, 260, 51, 438, 199, 375, 47, 339, 136, 225, 105, 288, 50, 341, -1, 356, 55, 188, 8, 129, 81, -23, 31, 46, 67, 46, 25, -37, -11, 74, 42, 121, 31, -20, 30, 32, 12, 125, 77, 51, 11, 65, 10, 154, 18, 111, 10, 75, -37, -1, -86, 65, -76, 65, -111, 133, -89, 116, -144, 82, -115, 144, -167, 49, -145, 17, -56, 96, -19, 145, -144, 33, -82, 3, 12, 159, 2, -85, -87, 225, -28, 119, -23, 61, -65, -19, -71, 57, -58, 47, -105, 42, -67, 71, -95, 201, 47, 212, -146, 117, -45, 168, 128, 96, 244, 268, 31, -53, -21, -149, -45, -180, -42, -183, -28, -135, -54, -93, -53, -207, -114, -86, -48, -238, -36, -121, -73, -100, -112, -154, -9, -242, -44, -40, -122, 134, 82, 101, 53, 95, 85, 199, 58, 174, -8, 86, -33, 92, 147, 111, 44, 91, 63, 84, 108, 180, -15, 118, 88, 92, -33, 102, -278, -103, -16, 60, -47, -68, -153, -20, -161, -85, -57, -25, -48, 69, -7, -8, -81, 18, -114, 28, -135, 31, -192, -28, -110, -56}
, {257, 466, 384, 287, 427, 332, 303, 341, 297, 208, 344, 371, 343, 406, 321, 396, 262, 427, 289, 453, 272, 468, 290, 356, 277, 407, 329, 103, 258, -17, 134, -184, -33, -236, 54, -146, 172, -166, 194, -47, 158, 81, 133, -10, 125, -68, -10, -124, -49, 0, -8, -116, 46, -112, 42, 212, 130, -87, 28, -146, 108, -99, 180, -110, 78, -106, -40, -74, -27, -85, 26, 68, -98, 149, -27, 68, -16, 132, 82, -44, -158, 15, -94, 30, 99, 97, 12, 7, 44, -17, 163, 59, 60, 68, 60, 23, 85, 34, 123, 89, 119, 49, 52, 79, 147, 68, 127, 82, 70, 103, 135, 87, 89, 127, 33, 54, 41, 95, 82, 53, 105, 59, 41, 38, 79, -46, 71, 44, -3, 49, 68, 120, 83, -35, 106, 57, -27, -14, -98, -60, -46, -48, 135, 9, -8, -79, -16, 7, -8, 20, 1, -75, 27, -65, 0, 88, 22, 11, -56, -133, 46, -76, 31, 48, -31, -70, -41, -98, 13, 26, -20, -77, -40, -29, -58, -81, -112, -8, -31, -30, -90, 21, 4, 3, -60, 9, 2, -78, 27, -104, -63, 6, -101, -60, -98, -52, -85, -72, -98, 4, -347, 40, -292, -96, -275, -100, -202, -123, -235, -117, -176, -168, -378, -89, -320, -138, -201, -120, -305, -162, -313, -155, -166, -107, -82, -86, 36, 138, -134, -74, -61, 31, 97, -41, -81, -53, -111, -60, -81, -108, -159, -64, -178, -104, -27, -77, 23, 88, 33, -36, -38, -144}
, {-114, -338, -134, -212, -361, -410, -347, -279, -464, -449, -466, -393, -506, -453, -658, -428, -482, -348, -472, -379, -420, -440, -500, -415, -534, -486, -544, -350, -15, 104, -116, 216, -137, 121, -137, -54, -153, -91, -257, -297, -169, -75, -183, -150, -199, -90, -209, -186, -176, -176, -200, -239, -216, -241, -235, -196, 16, 80, -123, 38, -181, 47, -169, 88, -167, 21, -318, -44, -243, -321, -282, -362, -131, -603, -34, -320, -118, -385, 98, -489, -174, -414, -259, -258, -147, 6, -197, -4, -371, 8, -422, -51, -299, 50, -417, -176, -487, -153, -335, -125, -266, -230, -357, -338, -415, -256, -568, -139, -627, -264, -387, -388, -541, -300, -81, -146, 58, -117, -75, -102, 80, -190, -63, -30, 177, 39, 28, 64, -58, -168, -114, -209, -134, -155, 36, -220, -68, -167, 19, 27, -116, -96, -33, 91, -18, 201, -111, 174, -90, 40, -106, 185, -282, 117, -205, -8, -171, 20, -85, -8, -244, 114, -233, -50, -370, 50, 96, 106, 34, -19, 145, -134, 37, -18, 69, -25, 46, -137, 90, -104, 77, -107, 131, -102, 28, -47, 93, -77, 162, -74, 85, -38, 102, -58, 136, -24, 185, -38, 110, -184, 105, -77, 16, -78, 83, -53, 76, -108, 23, -54, 72, -184, 21, -184, 113, -85, 60, -65, 24, -71, 63, -108, 82, -123, 66, -171, 170, 452, -38, -69, -20, 5, 83, 6, 21, -37, -45, 12, 72, -61, 60, 7, -28, -53, 12, -27, 18, -99, 68, -118, 56, -47}
, {-312, -307, -467, -363, -458, -479, -434, -396, -413, -437, -381, -512, -456, -435, -549, -495, -488, -463, -485, -539, -520, -547, -485, -609, -539, -483, -424, -389, -156, -273, 72, -313, 70, -362, -89, -284, -66, -40, -109, -114, -302, -276, -197, -396, -284, -477, -381, -366, -390, -473, -293, -345, -516, -213, -439, -223, -122, 84, 97, 178, 31, 6, 106, 39, 91, -148, 285, -85, 81, -63, 54, 43, -86, -81, -172, -72, -215, -230, -326, -120, -123, -109, -268, 31, -100, -160, -20, -55, 77, -174, 136, -153, 17, -218, -98, -69, -46, -19, -81, -117, -83, -126, 35, -141, -43, -95, 27, -205, -11, -243, -100, -21, -23, -70, 43, -137, 13, -243, 110, -269, -26, -203, 7, -422, -157, -298, -63, -408, -49, -208, -24, -205, -118, -218, -270, -158, -225, -369, -474, -339, -369, -142, -344, 98, -293, -150, -374, 37, -297, 84, -241, -83, -373, -95, -263, -13, -468, -38, -443, -80, -272, -41, -506, -160, -330, -248, 37, -113, -42, -347, 136, -56, 163, -96, 75, -42, 57, -7, 85, 1, 117, -45, 170, -12, 111, -102, 45, -110, 26, -80, 73, -70, 17, -96, 40, -17, 135, -52, 72, -145, -334, 57, -204, -18, -196, 4, -288, 60, -334, 41, -334, 5, -241, 93, -322, 58, -230, 49, -228, 99, -254, 62, -258, 121, -262, 65, -301, -441, -53, 17, -73, -45, -48, 44, -97, -55, -63, -57, -80, 73, -15, 1, -46, -41, -88, -7, -87, 88, -43, -8, 19, 15}
, {-25, -11, -28, -14, -14, -15, -109, 65, 10, -53, -23, 26, -63, 83, -96, 43, -28, 26, 7, -78, -69, -4, -51, -54, 20, 15, 3, 4, 14, 46, 103, 4, -66, -3, -68, -44, -50, -115, -22, 2, -90, 21, -163, -14, -166, 105, -167, 61, -208, -1, -158, 67, -16, 3, -61, 216, -38, 141, -4, 164, 19, 88, -45, 86, 21, 79, 67, 74, -53, 29, 36, 27, -15, 5, -13, 87, -81, 52, 74, -2, 54, 73, 16, 80, 95, 21, 97, -33, 36, -46, 72, 160, -5, 4, 6, -32, 74, 55, 127, 16, 136, -53, 5, -30, 38, 45, 97, 115, 60, 87, 66, 109, -52, -111, 86, -213, -54, -207, -73, -187, 7, -157, -67, -72, -104, -108, -145, -57, 57, -142, -95, -34, -92, -39, 40, -158, -13, -70, 8, -80, 70, -27, -46, 25, -2, -85, 39, -81, -46, -76, -163, -102, -75, -27, -169, 60, -125, 25, -124, 13, -21, -25, -59, -42, -44, 32, -274, -28, -199, 39, -141, 172, -241, 71, -119, 76, -236, 148, -253, 35, -213, 107, -117, 88, -139, 50, -219, 121, -209, 96, -237, 95, -198, 214, -97, 57, -140, 137, -17, 106, 152, 74, 233, 30, 193, 101, 231, 37, 156, 46, 261, 157, 250, 171, 194, 59, 161, 30, 215, 152, 174, 90, 155, 98, 192, 40, 171, 62, 119, 156, 96, 135, 48, 14, 56, 39, 91, 104, 76, 48, 141, 146, 156, 135, 90, 31, 118, 144, 166, 40, 110, 101}
}
;

#undef INPUT_SAMPLES
#undef FC_UNITS
/**
  ******************************************************************************
  * @file    model.hh
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version 1.0.0
  * @date    08 july 2020
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */


#ifdef __cplusplus
extern "C" {
#endif

#ifndef __MODEL_H__
#define __MODEL_H__

#ifndef SINGLE_FILE
#include "number.h"

 // InputLayer is excluded
#include "conv1d.h" // InputLayer is excluded
#include "flatten.h" // InputLayer is excluded
#include "dense.h"
#endif


#define MODEL_INPUT_DIM_0 128
#define MODEL_INPUT_DIM_1 9
#define MODEL_INPUT_DIMS 128 * 9

#define MODEL_OUTPUT_SAMPLES 6

#define MODEL_INPUT_SCALE_FACTOR 9 // scale factor of InputLayer
#define MODEL_INPUT_ROUND_MODE ROUND_MODE_FLOOR
#define MODEL_INPUT_NUMBER_T int16_t
#define MODEL_INPUT_LONG_NUMBER_T int32_t

// node 0 is InputLayer so use its output shape as input shape of the model
// typedef  input_t[128][9];
typedef int16_t input_t[128][9];
typedef dense_output_type output_t;


void cnn(
  const input_t input,
  output_t output);

void reset(void);

#endif//__MODEL_H__


#ifdef __cplusplus
} // extern "C"
#endif
/**
  ******************************************************************************
  * @file    model.cc
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version 1.0.0
  * @date    24 march 2020
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */

#ifdef __cplusplus
extern "C" {
#endif

#ifndef SINGLE_FILE
#include "number.h"
#include "model.h"
// #include <chrono>

 // InputLayer is excluded
#include "conv1d.c"
#include "weights/conv1d.c" // InputLayer is excluded
#include "flatten.c" // InputLayer is excluded
#include "dense.c"
#include "weights/dense.c"
#endif


void cnn(
  const input_t input,
  dense_output_type dense_output) {
  
  // Output array allocation
  static union {
    conv1d_output_type conv1d_output;
    flatten_output_type flatten_output;
  } activations1;


// Model layers call chain 

  //TODO: Fill model layer calls chain

}

#ifdef __cplusplus
} // extern "C"
#endif
