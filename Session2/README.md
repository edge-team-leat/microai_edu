Zephyr project for EDU32F103 v0.4 board including configurations for VS Code.

This Git repository must be cloned and every step must be performed from inside the Session2 folder.
Alternatively, if you are not familiar with Git, you can download and extract an archive of the repository from https://bitbucket.org/edge-team-leat/microai_edu/get/master.zip .

## Requirements

### Ubuntu (20.04)

Add the [Kitware APT repository](https://apt.kitware.com/) in order to install a more recent CMake version (3.20.0 or newer required):
```
wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | sudo tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null
echo 'deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] https://apt.kitware.com/ubuntu/ focal main' | sudo tee /etc/apt/sources.list.d/kitware.list >/dev/null
sudo apt-get update
```

Then follow the steps for Ubuntu 22.04 thereafter.
The `west update` commands will have to be performed without the `--fetch-opt=--filter=tree:0` argument since partial clones are not supported properly on older Git versions.

### Ubuntu (22.04 or newer) local installation
The local installation will the Zephyr tools, SDK and source repository inside the current folder for use by a single user.

Install required system tools:
```
sudo apt install -y cmake ninja-build python3-pip python3-venv git device-tree-compiler wget openocd
```

Create and activate a Python virtual environment:
```
python3 -m venv .venv
source .venv/bin/activate
```

Install West, pyelftools and anytree:
```
pip install west pyelftools anytree
```

Install Zephyr SDK with ARM toolchain:
```
wget https://github.com/zephyrproject-rtos/sdk-ng/releases/download/v0.17.0/zephyr-sdk-0.17.0_linux-x86_64_minimal.tar.xz
tar -xvf zephyr-sdk-0.17.0_linux-x86_64_minimal.tar.xz
pushd zephyr-sdk-0.17.0
./setup.sh -t arm-zephyr-eabi -h -c
popd
```

Initialize workspace:
```
west init -l manifest
```

Clone workspace dependencies (zephyr, cmsis, stm32_hal):
```
west update --fetch-opt=--filter=tree:0
```

**Warning**: make sure there is no whitespace in the path where the workspace is configured.

### Ubuntu (22.04 or newer) system installation

Alternatively, the Zephyr tools, SDK and source repository can be installed on the system to be available for all users.

Install required system tools:
```
sudo apt install -y cmake ninja-build git wget python3-pip python3-pyelftools python3-pip python3-packaging python3-pykwalify python3-dateutil python3-colorama python3-yaml python3-setuptools
```

Install West and anytree:
```
sudo pip install west anytree
```

Install Zephyr SDK with ARM toolchain:
```
wget https://github.com/zephyrproject-rtos/sdk-ng/releases/download/v0.17.0/zephyr-sdk-0.17.0_linux-x86_64_minimal.tar.xz
sudo tar -C /opt -xvf zephyr-sdk-0.17.0_linux-x86_64_minimal.tar.xz
sudo /opt/zephyr-sdk-0.17.0/setup.sh -t arm-zephyr-eabi -h
```

Initialize workspace:
```
sudo mkdir /opt/zephyr
sudo cp -r manifest /opt/zephyr
sudo west init -l /opt/zephyr/manifest
```

Clone workspace dependencies (zephyr, cmsis, stm32_hal):
```
pushd /opt/zephyr
sudo west update --fetch-opt=--filter=tree:0
popd
```

Enable workspace use by all users:
```
sudo git config --system --add safe.directory /opt/zephyr/zephyr
echo "export ZEPHYR_BASE=/opt/zephyr/zephyr" | sudo tee /etc/profile.d/zephyr.sh
source /etc/profile.d/zephyr.sh
sudo -i west config zephyr.base zephyr
```

Install udev rules for user USB access:
```
sudo ln -s /opt/zephyr-sdk-0.17.0/sysroots/x86_64-pokysdk-linux/usr/share/openocd/contrib/60-openocd.rules /etc/udev/rules.d/
sudo udevadm control --reload
```

In order for the VS Code project to work with the system installation, some paths must be edited:
```
sed "s|.venv/bin/python|python3|g" -i .vscode/tasks.json
sed "s|\${workspaceRoot}/zephyr|/opt/zephyr|" -i .vscode/launch.json
```

### Windows

Execute `InstallTools.cmd` to install the following required system tools (as administrator):

- Chocolatey
 - CMake
 - Ninja
 - Python3.11
 - Git
 - DTC
 - Wget
 - OpenOCD
 - EDU32F103 driver

Make sure to log out and log back in to Windows or reboot the machine in order to refresh the PATH environment variable after running `InstallTools.cmd`.

Execute `ConfigureWorkspace.cmd` to set up the local workspace (as user):

 - Create a Python virtual environment
 - Install West
 - Install Zephyr SDK with ARM toolchain
 - Initialize workspace
 - Clone workspace dependencies (zephyr, cmsis, stm32_hal)
 - Install pyelftools

**Warning**: make sure there is no whitespace or special (e.g., accentuated) characters in the path where the workspace is configured.

### VS Code extensions
Extensions are suggested when first opening the project, otherwise call the "Extensions: Show Recommended Extensions" command with Ctrl+Shift+P, or install them manually from VSIX:

- Microsoft C/C++: https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools
- Microsoft Serial Monitor: https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-serial-monitor
- Marus Cortex-Debug: https://marketplace.visualstudio.com/items?itemName=marus25.cortex-debug
- Marus Cortex-Debug Device Support Pack STM32F1: https://marketplace.visualstudio.com/items?itemName=marus25.cortex-debug-dp-stm32f1

## How to use

### VS Code

#### Open the project in VS Code

- Start VS Code
- Open this directory
- Install the recommended extensions

#### Build and run with VS Code

- Make sure the EDU32F103 board is plugged in to the USB port, you may need to enter bootloader mode (press and hold `BOOT0` then press and release `RST#`, finally release `BOOT0` after a couple of seconds)
- Click `Start Debugging` in the `Run` menu, this will compile the source code, send the binary to the MCU, start it, and open the debugger

#### Visualize the serial output

- In VS Code, click `New Terminal` in the `Terminal`
- Select the `Serial Monitor` tab in the bottom terminal window
- Select the correct serial port (e.g., `/dev/ttyUSB1`)
- Make sure baud rate is set to 115200
- Click `Start Monitoring`

### Command line tools

#### Activate Python virtual environment
On Linux (skip if using system installation):
```
source .venv/bin/activate
```

On Windows:
```
Set-ExecutionPolicy Bypass -Scope Process -Force
.\.venv\Scripts\Activate.ps1
```

#### Build project
```
west build -b edu32f103
```

#### Flash firmware to the target
```
west flash
```

#### Debug project on the target
```
west debug
```

#### Read serial output
On Linux:
```
minicom -D/dev/ttyUSB1
```

## Troubleshooting

### `.venv/bin/python: The term '.venv/bin/python' is not recognized as the name of a cmdlet, function, script, file, or operable program.`

This means the local Python virtual environment is not accessible.
On Windows, make sure the `ConfigureWorkspace.cmd` script ran properly.
On Linux, make sure you have created a virtual environment in the correct directory (Session2).

### `New-Item: Incorrect Function`, `Could not create directory junction!`

If this happens when running `ConfigureWorkspace.cmd` for the `New-Item -ItemType Junction -Path "bin" -Target "Scripts"` command,
this means the filesystem where the directory is stored is unsupported.
This can happen when running from an external drive partition formatted as FAT32 or ExFAT.
Make sure to run the script from an NTFS filesystem (e.g., the internal primary drive partition `C:`).

### `fatal: detected dubious ownership in repository at '…'`, `'[…]' is on a file system that does not record ownership`, `FATAL ERROR: command exited with status 128: remote add […]`

This means the `ConfigureWorkspace.cmd` script has been executed from a directory on an unsupported filesystem (such as FAT32 or ExFAT).
Make sure to run the script from an NTFS filesystem (e.g., the internal primary drive partition `C:`).

A similar error can happen if the script has been run first with administrator privileges, then again without administrator privileges.
This script should *not* be executed as administrator. Delete the `zephyr/` and `modules/` directory then try again withou administrator privileges.

### `UnicodeDecodError: 'utf-8' codec can't decode byte … in position …: invalid continuation byte`
Make sure the workspace path does not contain special (e.g., accentuated) characters.
