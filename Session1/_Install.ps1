Add-Type -Assembly "System.IO.Compression.Filesystem"

# Chocolatey
"Installing Chocolatey."
Set-ExecutionPolicy Bypass -Scope Process -Force
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072
Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
if ($?) {
    "Chocolatey installed."
} else {
    Write-Error "Chocolatey installation failed!"
}

# Arm GNU Toolchain
"Installing Arm GNU Toolchain."
choco install -y gcc-arm-embedded
if ($?) {
    "Arm GNU Toolchain installed."
} else {
    Write-Error "Arm GNU Toolchain installation failed!"
}

# Make
"Installing Make."
choco install -y make
if ($?) {
    "Make installed."
} else {
    Write-Error "Make installation failed!"
}

# OpenOCD
"Installing OpenOCD."
choco install -y openocd
if ($?) {
    "OpenOCD installed."
} else {
    Write-Error "OpenOCD installation failed!"
}

# cURL
"Installing cURL."
choco install -y curl
if ($?) {
    "cURL installed."
} else {
    Write-Error "cURL installation failed!"
}

$userAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:129.0) Gecko/20100101 Firefox/129.0"
$curl = "C:\ProgramData\chocolatey\bin\curl.exe"

# STM32CubeF1
$stmRepoPath = "$env:USERPROFILE\STM32Cube\Repository"
if (Test-Path -Path "$stmRepoPath\STM32Cube_FW_F1_V1.8.6") {
    "Skipping STM32CubeF1 Firmware package installation: $stmRepoPath\STM32Cube_FW_F1_V1.8.6 already exists"
} else {
    "Installing STM32CubeF1 Firmware package."
    New-Item -ItemType Directory -Path "$stmRepoPath" -Force
    # Copy from current directory, by default try to download from ST server below
    #Copy-Item -Path ".\en.stm32cubef1.zip" -Destination "$stmRepoPath\stm32cube_fw_f1_v180.zip"
    #Copy-Item -Path ".\en.stm32cubef1-v1-8-6.zip" -Destination "$stmRepoPath\stm32cube_fw_f1_v186.zip"
    Push-Location "$stmRepoPath"
    "Downloading STM32CubeF1 v1.8.0."
    # Neither Invoke-WebRequest nor built-in curl support HTTP/2, required by ST server so use Chocolatey-installed curl instead
    # Invoke-WebRequest supports HTTP2 in Powershell 7.3.0, but 5.1 is installed by default
    #Invoke-WebRequest -UserAgent "$userAgent" -Headers @{"Accept-Encoding"="gzip"} -HttpVersion 2 -URI "https://www.st.com/content/ccc/resource/technical/software/firmware/40/db/b8/d5/bd/a7/41/b1/stm32cubef1.zip/files/stm32cubef1.zip/jcr:content/translations/en.stm32cubef1.zip" -OutFile "stm32cube_fw_f1_v180.zip"
    & $curl -C - -H "User-Agent: ${userAgent}" -H "Cookie: ak_es_cc=fr" -H "Referer: https://www.st.com/" --compressed --http2 "https://www.st.com/content/ccc/resource/technical/software/firmware/40/db/b8/d5/bd/a7/41/b1/stm32cubef1.zip/files/stm32cubef1.zip/jcr:content/translations/en.stm32cubef1.zip" -o "stm32cube_fw_f1_v180.zip"
    "Downloading STM32CubeF1 Patch v1.8.6"
    #Invoke-WebRequest -UserAgent $userAgent -Headers @{"Accept-Encoding"="gzip"} -HttpVersion 2 -URI "https://www.st.com/content/ccc/resource/technical/software/firmware/group2/3c/0d/fe/27/20/b3/4e/9a/stm32cubef1-v1-8-6/files/stm32cubef1-v1-8-6.zip/jcr:content/translations/en.stm32cubef1-v1-8-6.zip" -OutFile "stm32cube_fw_f1_v186.zip"
    & $curl -C - -H "User-Agent: ${userAgent}" -H "Cookie: ak_es_cc=fr" -H "Referer: https://www.st.com/" --compressed --http2 "https://www.st.com/content/ccc/resource/technical/software/firmware/group2/3c/0d/fe/27/20/b3/4e/9a/stm32cubef1-v1-8-6/files/stm32cubef1-v1-8-6.zip/jcr:content/translations/en.stm32cubef1-v1-8-6.zip" -o "stm32cube_fw_f1_v186.zip"

    ## Unzip
    # Expand-Archive is way too slow, use ExtractToDirectory instead and merge manually since built-in dotnet 4 does not support overwriting existing files
    #Expand-Archive -Path stm32cube_fw_f1_v180.zip -DestinationPath .\ -Force
    #Expand-Archive -Path stm32cube_fw_f1_v186.zip -DestinationPath .\ -Force

    "Extracting stm32cube_fw_f1_v186.zip"
    [System.IO.Compression.ZipFile]::ExtractToDirectory("$stmRepoPath\stm32cube_fw_f1_v186.zip", $stmRepoPath)
    # Directory extracted from stm32cube_fw_f1_v186.zip is called STM32Cube_FW_F1_V1.8.0 instead of STM32Cube_FW_F1_V1.8.6, rename
    Rename-Item -Path ".\STM32Cube_FW_F1_V1.8.0" -NewName "STM32Cube_FW_F1_V1.8.6"

    "Extracting stm32cube_fw_f1_v180.zip"
    [System.IO.Compression.ZipFile]::ExtractToDirectory("$stmRepoPath\stm32cube_fw_f1_v180.zip", $stmRepoPath)

    # Merge STM32Cube_FW_F1_V1.8.0 into STM32Cube_FW_F1_V1.8.6, excluding existing files
    "Merging STM32Cube_FW_F1_V1.8.0 into STM32Cube_FW_F1_V1.8.6."
    robocopy STM32Cube_FW_F1_V1.8.0 STM32Cube_FW_F1_V1.8.6 /E /XC /XN /XO /NFL /NDL /MOVE

    # Cleanup base 1.8.0 firmware directory to avoid conflict with the updated 1.8.6 firmware
    "Removing STM32Cube_FW_F1_V1.8.0"
    Remove-Item -Path STM32Cube_FW_F1_V1.8.0 -Recurse -Force

    Pop-Location
    "STM32CubeF1 firmware package installed to $stmRepoPath\STM32Cube_FW_F1_V1.8.6"
}

# STM32CubeMX
"Downloading STM32CubeMX v6.13.0."
& $curl -C - -H "User-Agent: ${userAgent}" -H "Cookie: ak_es_cc=fr" -H "Referer: https://www.st.com/" --compressed --http2 "https://www.st.com/content/ccc/resource/technical/software/sw_development_suite/group1/51/f8/78/7d/b9/b8/4e/98/stm32cubemx-win-v6-13-0/files/stm32cubemx-win-v6-13-0.zip/jcr:content/translations/en.stm32cubemx-win-v6-13-0.zip" -O
"Extracting en.stm32cubemx-win-v6-13-0.zip"
[System.IO.Compression.ZipFile]::ExtractToDirectory($((pwd).tostring()+"\en.stm32cubemx-win-v6-13-0.zip"), $(pwd))
"Installing STM32CubeMX"
.\SetupSTM32CubeMX-6.13.0-Win.exe $((pwd).tostring()+"\auto-install.xml")
if ($?) {
    Write-Warning  "STM32CubeMX is currently being installed in the background. Please wait for it to finish."
} else {
    Write-Error "STM32CubeMX installation failed!"
}

# Install Driver
"Downloading EDU32F103 driver: InstallDriver_EDU32F103_v0.4-2_silent.exe"
Invoke-WebRequest -URI "https://bitbucket.org/edge-team-leat/microai_edu/downloads/InstallDriver_EDU32F103_v0.4-2_silent.exe" -OutFile InstallDriver_EDU32F103_v0.4-2_silent.exe
"Installing EDU32F103 v0.4 driver."
.\InstallDriver_EDU32F103_v0.4-2_silent.exe
if ($?) {
    "EDU32F103 v0.4-2 driver installed."
} else {
    Write-Error "EDU32F103 v0.4-2 driver installation failed!"
}
